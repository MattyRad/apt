#!/bin/bash

# curl https://gitlab.com/MattyRad/apt/-/raw/master/install.sh | bash -s vim

# "stock" regular terminal based use
# "desktop" window apps

for arg in $@
do
    # Util
    if [ $arg == 'util' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y curl
    fi

    # Terminal

    ## Vim
    if [ $arg == 'vim' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y vim
    fi
    if [ $arg == 'neovim' ]; then
        sudo apt-get install -y neovim
    fi
    if [ $arg == 'vim-extras' ]; then
        sudo apt-get install -y gvim
        sudo apt-get install -y vim-gtk3
    fi

    ## Micro
    if [ $arg == 'micro' ] || [ $arg == 'stock' ]; then
        curl https://getmic.ro | bash
        sudo chmod +x ./micro
        sudo mv ./micro /usr/local/bin
        micro -plugin install fzf
        rm ~/.config/micro/bindings.json
        ln -s ~/dotfiles/micro/bindings.json ~/.config/micro/bindings.json
    fi

    ## Git
    if [ $arg == 'git' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y git
    fi

    ## Tmux
    if [ $arg == 'tmux' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y tmux
    fi

    ## Zsh
    if [ $arg == 'zsh' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y zsh
        #sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
        #chsh -s $(which zsh)
    fi

    ## Fish
    if [ $arg == 'fish' ] || [ $arg == 'stock' ]; then
        sudo apt-add-repository ppa:fish-shell/release-3
        sudo apt-get update
        sudo apt-get install fish
    fi

    ## Dotfiles
    if [ $arg == 'dotfiles' ]; then
        if [ ! -d ~/dotfiles ]; then
            git clone https://github.com/MattyRad/dotfiles.git ~/dotfiles

            echo "
if [ -f ~/.zshrc.extended ]; then
    source ~/.zshrc.extended
else
    print "~/.zshrc.extended not found"
fi" >> ~/.zshrc

            echo "
if test -f ~/.fishrc.extended
    source ~/.fishrc.extended
else
    echo '~/.fishrc.extended not found'
end" >> ~/.config/fish/config.fish
            sed -i 's/plugins=(git)/plugins=(git zsh-syntax-highlighting zsh-autosuggestions)/' ~/.zshrc
        fi

        mkdir -p ~/.config/fish/functions
        mkdir -p ~/.config/micro

        ln -f -s ~/dotfiles/.vimrc ~/.vimrc
        ln -f -s ~/dotfiles/.zshrc ~/.zshrc.extended
        ln -f -s ~/dotfiles/fish/config.fish ~/.fishrc.extended
        ln -f -s ~/dotfiles/fish/fish_prompt.fish ~/.config/fish/functions

        cd
        #if [ -f "$PWD/.config/micro/bindings.json" ]; then
            #(cat ~/.config/micro/bindings.json | jq '. * {"Ctrl-w": "Quit", "CtrlShiftUp": "MoveLinesUp", "CtrlShiftDown": "MoveLinesDown"}') > bindings.tmp
            #mv bindings.tmp ~/.config/micro/bindings.json
            rm ~/.config/micro/bindings.json
        #else
            ln -s ~/dotfiles/micro/bindings.json ~/.config/micro
        #fi
        rm -rf ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting && git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
        rm -rf ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions && git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
        rm -rf ~/.oh-my-zsh/custom/plugins/fzf-tab && git clone https://github.com/Aloxaf/fzf-tab ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/fzf-tab
    fi

    # Rust

    if [ $arg == 'rust' ] || [ $arg == 'alacritty' ]; then
        curl https://sh.rustup.rs -sSf | sh -s -- -y
        ln -s ~/dotfiles/alacritty.yml ~/.alacritty.yml
    fi

    ## Alacritty
    if [ $arg == 'desktop' ] || [ $arg == 'alacritty' ]; then
        # this seg faults...
        # snap install alacritty --classic

        mkdir -p ~/projects/other
        git clone https://github.com/alacritty/alacritty ~/projects/other/alacritty
        #cd ~/projects/other/alacritty && cargo install alacritty
        cd ~/projects/other/alacritty && cargo build --release
        sudo mv ./target/release/alacritty /usr/local/bin/
        cd

        # set as default
        #sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/bin/alacritty 50
    fi

    ## zoxide
    if [ $arg == 'zoxide' ] || [ $arg == 'stock' ]; then
        #curl -sS https://webinstall.dev/zoxide | bash

        mkdir zoxide
        cd zoxide
        wget https://github.com/ajeetdsouza/zoxide/releases/download/v0.8.1/zoxide-v0.8.1-x86_64-unknown-linux-musl.tar.gz -O zoxide.tar.gz
        tar -zxvf zoxide.tar.gz
        chmod +x zoxide
        sudo mv zoxide /usr/local/bin/
        cd ..
        rm -r zoxide
    fi

    ## tilix
    if [ $arg == 'tilix' ]; then
        sudo apt-get install -y tilix
    fi

    ## ripgrep
    if [ $arg == 'ripgrep' ] || [ $arg == 'cli' ]; then
        wget https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb -O ripgrep.deb
        sudo dpkg -i ripgrep.deb
        rm ripgrep.deb
    fi

    ## fd
    if [ $arg == 'fd' ] || [ $arg == 'cli' ]; then
        mkdir fd
        cd fd
        wget https://github.com/sharkdp/fd/releases/download/v8.3.2/fd-v8.3.2-x86_64-unknown-linux-musl.tar.gz -O fd.tar.gz
        tar -zxvf fd.tar.gz
        chmod +x fd-v8.3.2-x86_64-unknown-linux-musl/fd
        sudo mv fd-v8.3.2-x86_64-unknown-linux-musl/fd /usr/local/bin/
        cd ..
        rm -r fd
    fi

    ## bat
    if [ $arg == 'bat' ] || [ $arg == 'cli' ]; then
        mkdir bat
        cd bat
        wget https://github.com/sharkdp/bat/releases/download/v0.20.0/bat-v0.20.0-x86_64-unknown-linux-musl.tar.gz -O bat.tar.gz
        tar -zxvf bat.tar.gz
        chmod +x bat-v0.20.0-x86_64-unknown-linux-musl/bat
        sudo mv bat-v0.20.0-x86_64-unknown-linux-musl/bat /usr/local/bin/
        cd ..
        rm -r bat
        # https://github.com/dandavison/delta/issues/162
        mkdir -p ~/.config/bat/syntaxes
        curl -L https://gist.githubusercontent.com/dasl-/9ea0d83cd478f36f6c7deaa737480492/raw/d2276cb45a41180304d7ad8b287b17c948c4d8a0/PHP.sublime-syntax > ~/.config/bat/syntaxes/PHP.sublime-syntax
        #bat cache --build
    fi

    ## exa
    if [ $arg == 'exa' ] || [ $arg == 'cli' ]; then
        mkdir exa
        cd exa
        wget https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-musl-v0.10.1.zip -O exa.zip
        unzip exa.zip
        chmod +x bin/exa
        sudo mv bin/exa /usr/local/bin/
        rm exa.zip
        cd ..
        rm -r exa
    fi

    ## skim
    if [ $arg == 'skim' ] || [ $arg == 'cli' ]; then
        wget https://github.com/lotabout/skim/releases/download/v0.9.4/skim-v0.9.4-x86_64-unknown-linux-musl.tar.gz -O skim.tar.gz
        tar -zxvf skim.tar.gz
        chmod +x sk
        sudo mv sk /usr/local/bin/
        rm skim.tar.gz
    fi

    ## zellij
    if [ $arg == 'zellij' ] || [ $arg == 'cli' ]; then
        wget https://github.com/zellij-org/zellij/releases/download/v0.26.1/zellij-x86_64-unknown-linux-musl.tar.gz -O zellij.tar.gz
        tar -zxvf zellij.tar.gz
        chmod +x zellij
        sudo mv zellij /usr/local/bin/
        rm zellij.tar.gz
        ln -s ~/dotfiles/zellij.yml ~/.config/zellij/config.yaml
    fi

    ## delta
    if [ $arg == 'delta' ] || [ $arg == 'cli' ]; then
        mkdir delta
        cd delta
        wget https://github.com/dandavison/delta/releases/download/0.12.1/delta-0.12.1-x86_64-unknown-linux-musl.tar.gz -O delta.tar.gz
        tar -zxvf delta.tar.gz
        sudo mv delta-0.12.1-x86_64-unknown-linux-musl/delta /usr/local/bin/
        cd ..
        rm -r delta
    fi

    ## dust
    if [ $arg == 'dust' ] || [ $arg == 'cli' ]; then
        mkdir dust
        cd dust
        wget https://github.com/bootandy/dust/releases/download/v0.8.1-alpha.2/dust-v0.8.1-alpha.2-x86_64-unknown-linux-musl.tar.gz -O dust.tar.gz
        tar -zxvf dust.tar.gz
        sudo mv dust-v0.8.1-alpha.2-x86_64-unknown-linux-musl/dust /usr/local/bin/
        cd ..
        rm -r dust
    fi

    ## Utility / Search tools
    if [ $arg == 'search' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y fzy colordiff jq tree silversearcher-ag autojump
    fi

    ## fzf
    if [ $arg == 'fzf' ] || [ $arg == 'stock' ]; then
        git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
        ~/.fzf/install
    fi

    # todo:
    # fd-find

    # Desktop

    ## Compiz
    if [ $arg == 'compiz' ] || [ $arg == 'desktop' ]; then
        sudo apt-get install -y gnome-session-flashback compiz compiz-core compiz-plugins compiz-plugins-default compiz-plugins-extra compiz-plugins-main compiz-plugins-main-default compiz-plugins-main-dev compizconfig-settings-manager
        gsettings set org.gnome.gnome-flashback.desktop.icons show-trash false
        gsettings set org.gnome.gnome-flashback.desktop.icons show-home false
    fi

    ## Docky
    if [ $arg == 'plank' ] || [ $arg == 'desktop' ]; then
        sudo apt-get install -y plank
        sudo apt-get install -y docky
    fi

    # Docker
    if [ $arg == 'docker' ] || [ $arg == 'docker-compose' ] || [ $arg == 'docker-compose-pi' ]; then
        curl -sSL https://get.docker.com | sh
        sudo usermod -a -G docker $USER
    fi
    if [ $arg == 'docker-compose' ]; then
        sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        sudo chmod +x /usr/local/bin/docker-compose
        #sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    fi
    if [ $arg == 'docker-compose-pi' ]; then
        sudo usermod -aG docker pi
        sudo apt-get install -y libffi-dev libssl-dev
        sudo apt-get install -y python3 python3-pip
        sudo apt-get remove python-configparser
        sudo pip3 install docker-compose
    fi

    # PHP

    if [ $arg == 'php' ]; then
        # mcrypt
        # phpize
        sudo apt-get install -y php php-xdebug php-zip php-dev php-xml php-curl php-mbstring php-bcmath php-ast php-intl php-gd php-imap
        # 8.0
        # sudo add-apt-repository ppa:ondrej/php
        # install ca-certificates apt-transport-https software-properties-common
        # sudo apt-get install -y php8.0 php8.0-xdebug php8.0-zip php8.0-dev php8.0-xml php8.0-curl php8.0-mbstring php8.0-bcmath php8.0-ast php8.0-intl php8.0-gd php8.0-imap
    fi

    ## Composer

    if [ $arg == 'php' ] || [ $arg == 'composer' ]; then
        EXPECTED_CHECKSUM="$(wget -q -O - https://composer.github.io/installer.sig)"
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

        if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
        then
            >&2 echo 'ERROR: Invalid installer checksum'
            rm composer-setup.php
            exit 1
        fi

        php composer-setup.php --quiet
        RESULT=$?
        rm composer-setup.php
        #exit $RESULT
        sudo mv composer.phar /usr/local/bin/composer
    fi

    # Sound and Video

    ## Spotify
    if [ $arg == 'spotify' ] || [ $arg == 'desktop' ]; then
        #curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -
        #curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
        curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add -
        echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
        sudo apt-get update
        sudo apt-get install spotify-client
    fi
    if [ $arg == 'spotify-extras' ]; then
        curl -kL https://github.com/tizonia/tizonia-openmax-il/raw/master/tools/install.sh | bash
        sudo python3 -m pip install spotify-cli-linux
        sudo pip install spotify-cli-linux
        sudo pip install lyricwikia
    fi

    ## VLC
    if [ $arg == 'vlc' ] || [ $arg == 'desktop' ]; then
        sudo apt-get install -y vlc
        sudo apt-get install -y ffmpegthumbnailer
    fi

    ## cmus
    if [ $arg == 'cmus' ]; then
        sudo apt-get install -y cmus
    fi

    ## musikcube
    if [ $arg == 'musikcube' ]; then
        sudo apt-get install -y libboost-system1.71.0 libboost-chrono1.71.0 libmicrohttpd12 libev4 libavcodec-extra

        wget https://github.com/clangen/musikcube/releases/download/0.93.1/musikcube_0.93.1_ubuntu_focal_amd64.deb -O musikcube.deb
        sudo dpkg -i musikcube.deb
        rm musikcube.deb
    fi

    ## moc
    if [ $arg == 'moc' ]; then
        sudo apt-get install -y moc
    fi

    # Sublime Text 3

    if [ $arg == 'sublime' ] || [ $arg == 'desktop' ]; then
        wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
        sudo apt-get install apt-transport-https
        echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
        sudo apt-get update
        sudo apt-get install sublime-text

        mkdir -p ~/.config/sublime-text/Packages
        git clone https://github.com/Suor/sublime-hippie-autocomplete.git ~/.config/sublime-text/Packages/sublime-hippie-autocomplete
    fi

    # Database

    ## Sqlite
    if [ $arg == 'sqlite' ] || [ $arg == 'stock' ]; then
        sudo apt-get install -y sqlite sqlite3
    fi

    ## Mysql
    if [ $arg == 'mysql' ]; then
        sudo apt-get install -y mysql-client
        sudo apt-get install -y mysql-server
    fi

    # Node

    if [ $arg == 'node' ]; then
        # Node installation has always been terrible, going off of this url this time
        # https://github.com/nodesource/distributions/blob/master/README.md#debinstall
        # who knows if it's actually what I want or if it'll work with my stuff
        sudo apt-get install -y build-essential
        curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
        sudo apt-get install -y nodejs
    fi

    # Random support libs

    if [ $arg == 'support' ] || [ $arg == 'stock' ]; then
        # cmake3
        # bat
        sudo apt-get install -y gcc g++ make libxcb-xtest0 gobject-introspection build-essential python3-dev libxcb-composite0-dev libx11-dev bridge-utils libproxy1-plugin-webkit ca-certificates libqt5opengl5 openssl ffmpeg libssl-dev cmake ncurses-base ncurses-dev xclip xsel libgeoip-dev libmaxminddb-dev
    fi

    # Monitoring

    if [ $arg == 'monitoring' ]; then
        sudo apt-get install -y glances htop
    fi

    # Fun

    if [ $arg == 'fun' ]; then
        sudo apt-get install -y lolcat cowsay fortune
    fi

    # Networking

    if [ $arg == 'network' ] || [ $arg == 'networking' ] || [ $arg == 'stock' ]; then
        # ncat
        sudo apt-get install -y net-tools ncaptool nmap
    fi

    ## VPN

    ### OpenConnect
    if [ $arg == 'openconnect' ]; then
        sudo apt-get install -y openconnect
    fi

    ### Syncthing
    if [ $arg == 'syncthing' ]; then
        mkdir syncthing
        cd syncthing
        wget https://github.com/syncthing/syncthing/releases/download/v1.19.1/syncthing-linux-amd64-v1.19.1.tar.gz -O syncthing.tar.gz
        tar -zxvf syncthing.tar.gz
        sudo mv syncthing-linux-amd64-v1.19.1/syncthing /usr/local/bin/
        cd ..
        rm -r syncthing
    fi

    # Python

    ## Pip
    if [ $arg == 'pip' ] || [ $arg == 'python' ]; then
        sudo apt-get install -y python3-pip
        # other:
        # python3-neovim
        # python-neovim
        # python-dev python-pip python3-dev python3-pip
        # python-apt
        # pip3 install pyvim
    fi

    # Java

    if [ $arg == 'java' ]; then
        sudo apt-get install -y openjdk-8-jre openjdk-8-jre-headless
    fi

    # Graphics

    ## Gimp
    if [ $arg == 'gimp' ] || [ $arg == 'graphics' ]; then
        sudo apt-get install -y gimp
    fi

    ## Graphviz
    if [ $arg == 'graphviz' ]; then
        sudo apt-get install -y graphviz
    fi

    ## Kazam
    if [ $arg == 'kazam' ]; then
        sudo apt-get install -y kazam
    fi

    # Fonts
    if [ $arg == 'fonts' ] || [ $arg == 'desktop' ]; then
        wget -O ecr.zip 'https://gitlab.com/MattyRad/apt/-/raw/master/EnvyCodeR-PR7.zip?inline=false'
        unzip ecr.zip
        sudo mv ./Envy\ Code\ R\ PR7 /usr/local/share/fonts/ecr
        rm ecr.zip

        wget -O hack.zip 'https://gitlab.com/MattyRad/apt/-/raw/master/Hack-v3.003-ttf.zip?inline=false'
        unzip hack.zip
        sudo mv ./ttf /usr/local/share/fonts/hack
        rm hack.zip
    fi

    # Primitive
    if [ $arg == 'primitive' ]; then
        # TODO separate golang
        wget https://golang.org/dl/go1.16.4.linux-amd64.tar.gz
        rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.4.linux-amd64.tar.gz
        rm go1.16.4.linux-amd64.tar.gz
        sudo ln -s /usr/local/go/bin/go /usr/local/bin
        go get -u github.com/fogleman/primitive
        # TODO export path
        # export PATH=$PATH:/usr/local/go/bin
        #~/go/bin/primitive -o ~/Pictures/image.jpeg -n 1500 -s 3840 -i <(wget -o /dev/null -O - -q 'https://reddit.com/r/earthporn/top.json' | jq -r '..|.url? | select(. and contains("jpg") and (contains("preview") or contains("static") | not))' | shuf | head -n 1 | xargs -I '{}' curl --silent "{}")
    fi

    #if [ $arg == 'fonts' ] || [ $arg == 'desktop' ]; then
    #   git clone https://github.com/ryanoasis/nerd-fonts.git
    #   cd nerd-fonts
    #   chmod 777 install.sh
    #   ./install.sh Hack
    #   cd ..
    #   rm -rf nerd-fonts
    #fi

done

sed -i 's/#DISABLE_UPDATE_PROMPT="true"/DISABLE_UPDATE_PROMPT="true"/' ~/.zshrc

# Run these manually!
#sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
#sed -i 's/robbyrussell/wedisagree/g' ~/.zshrc
#sudo apt-get install -y ubuntu-restricted-extras ubuntu-restricted-addons gnome-tweaks gnome-tweak-tool
#curl -sL https://git.io/fisher | source
#fisher install jorgebucaran/fisher
#this one is iffy; fisher install jethrokuan/fzf
#fisher install PatrickF1/fzf.fish

exit 0

# et cetera
# Search
#install ddgr
#
## Gnome
#install gconf-editor
#install oomox
#
## Unknown
#install wmctrl
#install awf
#install socat
#install inkscape librsvg2-bin optipng libsass0 sassc parallel gtk2-engines libxml2-utils


# TODO
# diodon
